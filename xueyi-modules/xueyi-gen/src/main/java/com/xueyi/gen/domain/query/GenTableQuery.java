package com.xueyi.gen.domain.query;

import com.xueyi.gen.domain.po.GenTablePo;

/**
 * 业务 数据查询对象
 *
 * @author xueyi
 */
public class GenTableQuery extends GenTablePo {

    private static final long serialVersionUID = 1L;

}