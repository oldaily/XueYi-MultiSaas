package com.xueyi.gen.domain.query;

import com.xueyi.gen.domain.po.GenTableColumnPo;

/**
 * 业务字段 数据查询对象
 *
 * @author xueyi
 */
public class GenTableColumnQuery extends GenTableColumnPo {

    private static final long serialVersionUID = 1L;

}