package com.xueyi.system.notice.domain.query;

import com.xueyi.system.notice.domain.po.SysNoticePo;

/**
 * 通知公告 数据查询对象
 *
 * @author xueyi
 */
public class SysNoticeQuery extends SysNoticePo {

    private static final long serialVersionUID = 1L;

}
