package com.xueyi.system.api.dict.domain.query;

import com.xueyi.system.api.dict.domain.po.SysConfigPo;

/**
 * 参数配置 数据查询对象
 *
 * @author xueyi
 */
public class SysConfigQuery extends SysConfigPo {

    private static final long serialVersionUID = 1L;

}
