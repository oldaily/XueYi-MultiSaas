package com.xueyi.system.api.dict.domain.query;

import com.xueyi.system.api.dict.domain.po.SysDictTypePo;

/**
 * 字典类型 数据查询对象
 *
 * @author xueyi
 */
public class SysDictTypeQuery extends SysDictTypePo {

    private static final long serialVersionUID = 1L;

}
