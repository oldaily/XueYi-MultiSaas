package com.xueyi.system.api.organize.domain.query;

import com.xueyi.system.api.organize.domain.po.SysEnterprisePo;

/**
 * 企业 数据查询对象
 *
 * @author xueyi
 */
public class SysEnterpriseQuery extends SysEnterprisePo {

    private static final long serialVersionUID = 1L;

}
