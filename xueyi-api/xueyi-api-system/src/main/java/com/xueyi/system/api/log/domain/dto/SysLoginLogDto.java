package com.xueyi.system.api.log.domain.dto;

import com.xueyi.system.api.log.domain.po.SysLoginLogPo;

/**
 * 访问日志 数据传输对象
 *
 * @author xueyi
 */
public class SysLoginLogDto extends SysLoginLogPo {

    private static final long serialVersionUID = 1L;

}
