package com.xueyi.system.api.organize.domain.query;

import com.xueyi.system.api.organize.domain.po.SysUserPo;

/**
 * 用户 数据查询对象
 *
 * @author xueyi
 */
public class SysUserQuery extends SysUserPo {

    private static final long serialVersionUID = 1L;

}
