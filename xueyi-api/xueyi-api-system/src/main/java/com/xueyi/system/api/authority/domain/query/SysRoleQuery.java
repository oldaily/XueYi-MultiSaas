package com.xueyi.system.api.authority.domain.query;

import com.xueyi.system.api.authority.domain.po.SysRolePo;

/**
 * 角色 数据查询对象
 *
 * @author xueyi
 */
public class SysRoleQuery extends SysRolePo {

    private static final long serialVersionUID = 1L;

}
