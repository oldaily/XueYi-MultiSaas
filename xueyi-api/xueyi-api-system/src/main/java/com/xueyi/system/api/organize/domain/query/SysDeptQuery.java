package com.xueyi.system.api.organize.domain.query;

import com.xueyi.system.api.organize.domain.po.SysDeptPo;

/**
 * 部门 数据查询对象
 *
 * @author xueyi
 */
public class SysDeptQuery extends SysDeptPo {

    private static final long serialVersionUID = 1L;

}
