package com.xueyi.job.api.domain.query;

import com.xueyi.job.api.domain.po.SysJobPo;

/**
 * 调度任务 数据查询对象
 *
 * @author xueyi
 */
public class SysJobQuery extends SysJobPo {

    private static final long serialVersionUID = 1L;

}
