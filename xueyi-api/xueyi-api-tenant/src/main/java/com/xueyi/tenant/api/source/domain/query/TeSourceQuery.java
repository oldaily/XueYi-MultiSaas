package com.xueyi.tenant.api.source.domain.query;

import com.xueyi.tenant.api.source.domain.po.TeSourcePo;

/**
 * 数据源 数据查询对象
 *
 * @author xueyi
 */
public class TeSourceQuery extends TeSourcePo {

    private static final long serialVersionUID = 1L;

}