package com.xueyi.tenant.api.tenant.domain.query;

import com.xueyi.tenant.api.tenant.domain.po.TeStrategyPo;

/**
 * 数据源策略 数据查询对象
 *
 * @author xueyi
 */
public class TeStrategyQuery extends TeStrategyPo {

    private static final long serialVersionUID = 1L;

}