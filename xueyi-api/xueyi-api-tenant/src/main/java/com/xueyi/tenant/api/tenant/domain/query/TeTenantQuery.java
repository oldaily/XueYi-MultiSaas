package com.xueyi.tenant.api.tenant.domain.query;

import com.xueyi.tenant.api.tenant.domain.po.TeTenantPo;

/**
 * 租户 数据查询对象
 *
 * @author xueyi
 */
public class TeTenantQuery extends TeTenantPo {

    private static final long serialVersionUID = 1L;

}